package ru.taximaster.www.presentation.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.kodein.di.generic.instance
import ru.taximaster.www.domain.available_orders_interactor.AvailableOrdersInteractor
import ru.taximaster.www.domain.shift_interactor.ShiftInteractor
import ru.taximaster.www.presentation.extension.call
import ru.taximaster.www.presentation.mapping.OrderMapper
import ru.taximaster.www.presentation.model.OrderModel
import ru.taximaster.www.presentation.util.event_bus.EventBus
import ru.taximaster.www.util.LoggingManager

/**
 * Вью модель для экрана свободных заказов
 */
class FreeOrdersViewModel(app: Application) : BaseViewModel(app) {

    /**
     * Интерактор водительских смен
     */
    private val shiftInteractor: ShiftInteractor by instance()

    /**
     * Интерактор заказов
     */
    private val ordersInteractor: AvailableOrdersInteractor by instance()

    /**
     * Event bus для взаимодействия с TmdService
     */
    private val eventBus: EventBus by instance()

    /**
     * Данные по списку заказов
     */
    var freeOrdersList: List<OrderModel>? = null
    private val _freeOrdersListUpdated = MutableLiveData<Void>()
    val freeOrdersListUpdated: LiveData<Void> = _freeOrdersListUpdated

    /**
     * Видимость списка заказов
     */
    private val _freeOrdersListVisibility = MutableLiveData<Boolean>()
    val freeOrdersListVisibility: LiveData<Boolean> = _freeOrdersListVisibility

    /**
     * Данные по видимости прогресс-бара
     */
    private val _progressVisibility = MutableLiveData<Boolean>()
    val progressVisibility: LiveData<Boolean> = _progressVisibility

    /**
     * Данные по видимости замечания о пустом списке заказов
     */
    private val _emptyListMsgVisibility = MutableLiveData<Boolean>()
    val emptyListMsgVisibility: LiveData<Boolean> = _emptyListMsgVisibility

    /**
     * Передача состояния смены водителя
     * @return на смене, если true; нет если false
     */
    private val _isOnShiftState = MutableLiveData<Boolean>()
    val isOnShiftState: LiveData<Boolean> = _isOnShiftState

    init {
        getShiftState()
        subscribeToShiftUpdates()
        subscribeToOrdersUpdates()
    }

    /**
     * Получение начального значения состояния смены
     */
    private fun getShiftState() {
        _isOnShiftState.value = shiftInteractor.isOnShift()
    }

    /**
     * Подписка на изменение водительских смен
     */
    private fun subscribeToShiftUpdates() {
        val disposable = shiftInteractor.shiftChangesEventSubject
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { isOnShift -> _isOnShiftState.value = isOnShift },
                { LoggingManager.handleException(it) }
            )
        compositeDisposable.add(disposable)
    }

    /**
     * Был тап по кнопке выхода на смену
     */
    fun onShiftButtonClicked() {
        shiftInteractor.startShift()
        subscribeToOrdersUpdates()
    }

    /**
     * Подписка на изменение списка заказов
     */
    private fun subscribeToOrdersUpdates() {
        if (shiftInteractor.isOnShift()) {
            eventBus.startGettingFreeOrders()
            val disposable = ordersInteractor.getFreeOrders()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    _progressVisibility.postValue(true)
                    _emptyListMsgVisibility.postValue(false)
                    _freeOrdersListVisibility.postValue(false)
                }
                .map { OrderMapper.mapOrderList(it) }
                .subscribe(
                    { orders ->
                        freeOrdersList = orders
                        _freeOrdersListUpdated.call()
                        _freeOrdersListVisibility.value = orders.isNotEmpty()
                        _emptyListMsgVisibility.value = orders.isEmpty()
                        _progressVisibility.value = false
                    },
                    { LoggingManager.handleException(it) }
                )
            compositeDisposable.add(disposable)
        }
    }

    override fun onCleared() {
        eventBus.stopGettingFreeOrders()
        super.onCleared()
    }
}