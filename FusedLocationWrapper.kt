package ru.taximaster.www.data.location.provider

import android.location.Location
import android.os.Looper
import com.google.android.gms.location.*
import org.kodein.di.Kodein
import org.kodein.di.generic.instance
import ru.taximaster.www.App
import ru.taximaster.www.data.mapping.LocationMapper
import ru.taximaster.www.data.repo.trip_params_repo.TripParamsRepo

/**
 * Класс-обертка для получения местоположения с помощью FusedLocationClient
 */
class FusedLocationWrapper(private val app: App) {

    companion object {
        private const val INTERVAL = 10_000L // интервал получения координат в секундах
        private const val MIN_INTERVAL = 5_000L // минимальный интервал получения координат в секундах
    }

    val kodein: Kodein by lazy { app.kodein }

    private val tripParamsRepo: TripParamsRepo by kodein.instance()

    private val locationClient: FusedLocationProviderClient by lazy {
        LocationServices.getFusedLocationProviderClient(app.applicationContext)
    }

    private var locationCallback: LocationCallback? = null

    /**
     * Начать получение текущих координат
     */
    @Throws(SecurityException::class)
    fun startGettingLocation() {
        initLocationListener()
        locationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                tripParamsRepo.setLocation(LocationMapper.mapLocation(location))
            }
        locationClient.requestLocationUpdates(getLocationRequest(), locationCallback, Looper.getMainLooper())
    }

    /**
     * Завершить получение текущих координат
     */
    fun stopGettingLocation() {
        locationCallback?.let { locationClient.removeLocationUpdates(it) }
    }

    /**
     * Инициализация слушателя местоположения
     */
    private fun initLocationListener() {
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations) {
                    tripParamsRepo.setLocation(LocationMapper.mapLocation(location))
                }
            }
        }
    }

    /**
     * Инициализация параметров запроса местоположения
     */
    private fun getLocationRequest(): LocationRequest? {
        return LocationRequest.create()?.apply {
            interval = INTERVAL
            fastestInterval = MIN_INTERVAL
            priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        }
    }
}